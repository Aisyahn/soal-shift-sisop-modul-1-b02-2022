#!/bin/bash
log=/home/aisy/modul1/log_website_daffainfo.log
folder=/home/aisy/modul1/forensic_log_website_daffainfo_log

#a
if [ ! -d "$folder" ]
then
	mkdir $folder
else
	rm -r $folder
	mkdir $folder
	echo "forensic_log_website_daffainfo_log was created"
fi

#b
cat $log | 
awk ' END {
	print "Rata-rata serangan adalah sebanyak ", (NR-1)/12, "requests per jam"}
' >> $folder/ratarata.txt

#c
cat $log |
awk -F: '{$1
	arr[$1]++}
	END {
		temp
		max=0
		for (i in arr){
			if(max < arr[i]){
				temp = i
				max = arr[temp]
			}
		}
		print "IP yang paling banyak mengakses server adalah: " temp " sebanyak " max " requests\n"}
' >> $folder/result.txt

#d
cat $log |
awk '/curl/ {++n}
	END {
	print "Ada " n " request yang menggunakan curl sebagai user-agent\n"}
' >> $folder/result.txt

#e
cat $log |
awk -F: '/2022:02/ {print $1 " Jam 2 pagi"}
' | uniq >>$folder/result.txt 
