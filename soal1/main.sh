#!/bin/bash


main(){
	read -p "Username : " username
	read -p "Password : " -s pw
	echo

	msg_login_success="LOGIN: INFO User $username logged in"
	msg_login_fail="LOGIN: ERROR Failed login attempt on user $username"
	
	NOW=$(date +"%m/%d/%y %T")
	printf "$NOW " >> log.txt
	printf "$NOW " 

	usernameExists=0
	loged_in=0	
	i=0
	while read -r line
	do
		i=$((i+1))
		if [ $((i % 2)) -eq 0 ]
		then
			if [ $((usernameExists)) == 1 ]
			then
				if [ "$pw" = "$line" ]
				then
					loged_in=1
				fi
				break
			else
				continue
			fi
		fi
		if [ "$username" = "$line" ]
		then
			usernameExists=1
			continue
		fi
		
	done < user.txt
	
	if [ $((loged_in)) == 1 ]
	then
		echo "$msg_login_success" >> log.txt
		echo "$msg_login_success"
		
		while true
		do
			read -p "cmd >> " cmd
			
			if [ "$cmd" = "att" ]
			then
				echo "Username : $username"
				awk -v awkvar="$msg_login_success" '
				$0 ~ awkvar { ++n }
				END { print "Login Success :", n, "kali" }' log.txt
				awk -v awkvar="$msg_login_fail" '
				$0 ~ awkvar { ++n }
				END { print "Login Failed  :", n, "kali" }' log.txt
			elif [ "${cmd:0:2}" = "dl" ]
			then	
				n=$((${cmd:3}))
				folder=$(date +"%Y-%m-%d")
				folder="${folder}_${username}"
				
				if test -f "${folder}.zip"
				then
					unzip -q -P $pw -q "${folder}.zip"
				else
					mkdir -p $folder
				fi
				
				cd $folder
				
				banyak_pic=$(find . -type f | wc -l)
				n=$((n+banyak_pic))
				while [ $banyak_pic -lt $n ]
				do
					banyak_pic=$((banyak_pic + 1))
					format=$(printf "PIC_%02d" $banyak_pic)
					if [ $banyak_pic -lt 10 ]
					then
						format="PIC_0$banyak_pic"
					else
						format="PIC_$banyak_pic"
					fi
					
					wget -q https://loremflickr.com/320/240 -O "$format"
					echo "Download Success >> $format"
				done
				cd -
				
				zip -q -P $pw -r "${folder}.zip" "$folder"
				rm -r "$folder"
			fi
			
			read -p "Continue (y/n) ? " cmd
			if [ "$cmd" = "y" ]
			then
				continue
			else
				break
			fi
		done
		
	else 
		echo "$msg_login_fail" >> log.txt
		echo "$msg_login_fail"
	fi


}

main
