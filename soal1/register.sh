#!/bin/bash
#register.sh

sign_in(){
	#to flag if registered succesfully or not
	state=1
	#read username & password
	read -p "Input Username >> " username
	
	#check the password
	pw=""
	while true
	do
		read -p "password >> " -s pw
		echo
		#Cek apakah mengandung lower,upper, dan alpha
		hasLower=0;hasUpper=0;hasAlpha=0
		for ((i=0; i<${#pw}; i++))
		do
			case "${pw:$i:1}" in
				([[:lower:]]) hasLower=1;;
				([[:upper:]]) hasUpper=1;;
				([[:alpha:]]) hasAlpha=1;;
			esac
		done
		#cek semua syarat password
		if [[ $hasLower == 1 && $hasUpper == 1 && $hasAlpha == 0  && ${#pw} -ge 8 && "$username" != "$pw" ]]
		then
			#password memenuhi
			break
		else
			echo "Password minimum 8 karakter, Memiliki huruf besar & kecil, non alphanumerics, & tidak sama dengan username"
			pw=""
		fi
	done

	#outputing the date and time
	NOW=$(date +"%m/%d/%y %T")
	printf "$NOW " >> log.txt
	printf "$NOW " 
	
	i=0
	while read -r line;
	do
		
		i=$((i + 1))
		if [ $((i % 2)) -eq 0 ] 
		then 
			continue
		fi

		if [ "$username" = "$line" ];then
			echo "Register: ERROR User already exists" >> log.txt
			echo "Register: ERROR User already exists"
			state=0
			break
		fi
	done < user.txt
	if [ $state -eq 1 ];then
		echo "Register: INFO User $username registered successfully" >> log.txt
		echo "Register: INFO User $username registered successfully"
		echo "$username" >> user.txt
		echo "$pw">>user.txt		
	fi
}

sign_in

