#!/bin/bash

minute_logger(){
	TIMESTAMP="$(date +'%Y%m%d%H%M%S')"
	format="metrics_$TIMESTAMP"

	cd ~/log

	free -m >> $format.log
	du -sh ~ >> $format.log

	chmod 373 $format.log
	exit
}


init_cron(){
	LOGPATH="/home/$USER/log"
	SCRIPTPATH="/home/$USER/log/script"

	mkdir -p $LOGPATH
	mkdir -p $SCRIPTPATH

	cp ./minute_log.sh $SCRIPTPATH/minute_log.sh
	cp ./aggregate_minutes_to_hourly_log.sh $SCRIPTPATH/aggregate_minutes_to_hourly_log.sh

	echo "* * * * * $SCRIPTPATH/minute_log.sh minute_logger" > mycron
	echo "0 * * * * $SCRIPTPATH/aggregate_minutes_to_hourly_log.sh" >> mycron

	crontab mycron
	rm mycron
}


${1:-init_cron}

