#!/bin/bash

get_mean(){
	mean=$(printf "%s\n" $input | awk '{ total += $1 } END { print total/NR }')
	memMean+=$mean" "
}


get_max(){
	max=$(printf "%s\n" $input | awk 'BEGIN{a=0}{if ($1>0+a) a=0+$1 fi} END{print a}')
	memMax+=$max" "
}


get_min(){
	min=$(printf "%s\n" $input | awk 'BEGIN{a=2^32}{if ($1<0+a) a=0+$1 fi} END{print a}')
	memMin+=$min" "
}

dump(){
	echo dumping site
}

hour_logger(){
	cd ~/log

	chmod 770 metrics_[0-9]*.log

	TIMESTAMP="$(date +'%Y%m%d%H')"
	format="metrics_agg_$TIMESTAMP"	
	touch $format.log

	declare -a memItem=("\$2" "\$3" "\$4" "\$5" "\$6" "\$7")
	declare -a swapItem=("\$2" "\$3" "\$4")

	for item2 in "Mem" "Swap"; do
		final_text=""
                memMean=""
                memMin=""
                memMax=""
		if [[ $item2 == "Mem" ]]
		then
			for item in ${memItem[@]}; do
                        	input=$(cat metrics_[0-9]*.log | grep $item2 | awk "{print $item}")
                        	get_mean
                        	get_max
                        	get_min
                	done
		elif [[ $item2 == "Swap" ]]
		then
			for item in ${swapItem[@]}; do
                        	input=$(cat metrics_[0-9]*.log | grep $item2 | awk "{print $item}")
                        	get_mean
                        	get_max
                        	get_min
                	done
		fi
		final_text+=$item2"_mean : "$memMean$'\n'
		final_text+=$item2"_max : "$memMax$'\n'
		final_text+=$item2"_min : "$memMin$'\n'
		echo "$final_text" | column -t >> $format.log
		#echo ""
        done
	rm metrics_[0-9]*.log
	chmod 370 $format.log
}

${1:-hour_logger}





